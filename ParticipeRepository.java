package com.pfe.campingo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pfe.campingo.entities.Participe;



public interface ParticipeRepository extends JpaRepository<Participe, Long >{

}
