package com.pfe.campingo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.pfe.campingo.entities.Participe;
import com.pfe.campingo.repository.ParticipeRepository;

@Service
public class ParticipeServiceImpl implements ParticipeService{
	@Autowired
	private ParticipeRepository participeRepository;
	
	@Override
	public List<Participe> getAllParticipe() {
		
		return participeRepository.findAll();
	}

	@Override
	public Participe findParticipeById(Long id) {
		Optional<Participe> utOptional = participeRepository.findById(id);

		if (utOptional.isEmpty()) {
			return null;
		} else {
			return utOptional.get();
		}
	}

	@Override
	public Participe updateParticipe(Participe participe) {
		Optional<Participe> utOptional = participeRepository.findById(participe.getId());

		if (utOptional.isEmpty()) {
			return null;
		} else {
			
			return participeRepository.save(participe);
		}
	}

	@Override
	public void deleteParticipe(Long id) {
		participeRepository.deleteById(id);
		
	}

	@Override
	public Participe createParticipe(Participe participe) {
		return participeRepository.save(participe);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	

	

}
