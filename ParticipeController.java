package com.pfe.campingo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pfe.campingo.entities.Participe;
import com.pfe.campingo.services.ParticipeService;



@RestController
@RequestMapping("/participe")
public class ParticipeController {

	@Autowired
	private ParticipeService participeService;
 
	@GetMapping (path = "/all")
	public List<Participe> getAllevents() {
		return participeService.getAllParticipe();
	}
	
	@PostMapping(path = "/add")
	public Participe createParticipe(@RequestBody Participe participe) {
		return participeService.createParticipe(participe);
	}

	// localhost:8080/utilisateur/1
	@GetMapping(path = "/{id}") 
	public ResponseEntity<Participe> findParticipeById(@PathVariable Long id) {
		Participe  participe = participeService.findParticipeById(id);

		if (participe == null) {
			return new ResponseEntity<Participe>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<Participe>( participe, HttpStatus.OK);
		}
	}
	
	@PutMapping(path = "/update") 
	public Participe updateParticipe(@RequestBody Participe  participe) {
		return participeService.updateParticipe(participe);
	} 

	@DeleteMapping(path = "/{id}") 
	public void deleteParticipe(@PathVariable Long id) {
		participeService.deleteParticipe(id);
	}
	
	/* localhost:8080/utilisateur/findByFirstName/amin
	@GetMapping(path = "/findByFirstName/{firstName}") 
	public ResponseEntity<List<Utilisateur>> findUtilisateurByFirstName(@PathVariable String firstName) {
		List<Utilisateur> utilisateurs = utilisateurService.findByFirstName(firstname);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}
	// localhost:8080/utilisateur/findByFirstNameAndLastName/khalil
	@GetMapping(path = "/findByFirstNameAndLastName/{firstName}/{lastName}") 
	public ResponseEntity<List<Utilisateur>> findUtilisateurByFirstNameAndLastName(@PathVariable String firstName,
			@PathVariable String lastName) {
		List<Utilisateur> utilisateurs = utilisateurService.findByFirstNameAndLastName(firstName, lastName);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}
	 // localhost:8080/utilisateur/findByFirstNameAndLastNameWithRB
	@GetMapping(path = "/findByFirstNameAndLastNameWithRB")
	public ResponseEntity<List<Utilisateur>> findUtilisateurByFirstNameAndLastName(
			@RequestBody FirstNameAndLastNameRequest firstNameAndLastNameRequest) {
		List<Utilisateur> utilisateurs = utilisateurService.findByFirstNameAndLastName(
				firstNameAndLastNameRequest.getFirstName(), firstNameAndLastNameRequest.getLastName());

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}
	
	// localhost:8080/utilisateur/findByRoleTitre/ADMIN
	@GetMapping(path = "/findByRoleTitre/{titre}") 
	public ResponseEntity<List<Utilisateur>> findUtilisateurByRole(@PathVariable String titre) {
		List<Utilisateur> utilisateurs = utilisateurService.findByRolesTitre(titre);

		if (utilisateurs.isEmpty()) {
			return new ResponseEntity<List<Utilisateur>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Utilisateur>>(utilisateurs, HttpStatus.OK);
		}
	}
	
*/
}
