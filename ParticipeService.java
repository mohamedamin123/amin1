package com.pfe.campingo.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.pfe.campingo.entities.Participe;



public interface ParticipeService extends UserDetailsService{
	//CRUD 
	public List<Participe> getAllParticipe();
	public Participe findParticipeById(Long id);
	public Participe updateParticipe(Participe participe);
	public void deleteParticipe(Long id);
	public Participe createParticipe(Participe participe);
	
}
